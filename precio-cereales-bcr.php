<?php
/*
Plugin Name: TiW - Precio Cereales BRC
Plugin URI: 
Description: Obten los precios de los cereales de la BCR
Version: 1.0
Date: 22 November 2017
Author: Alan Cluet Saball - TiW
Author URI: http://www.tiw.cat
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


//ACTIVATION MAIN CODE
function tiw_precio_cereales_bcr_activate() {
   global $wpdb;

   $tabla_data_precios_cereales = $wpdb->prefix . "data_tiw_precios_cereales_bcr"; 
	$sql = "CREATE TABLE $tabla_data_precios_cereales (
	  id mediumint(9) NOT NULL AUTO_INCREMENT,
	  dia mediumint(9) NOT NULL,
	  trigo mediumint(9) NOT NULL,
	  sorgo mediumint(9) NOT NULL,
	  soja mediumint(9) NOT NULL,
	  maiz mediumint(9) NOT NULL,
	  PRIMARY KEY  (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );   
}

register_activation_hook( __FILE__, 'tiw_precio_cereales_bcr_activate' );


//DEACTIVATION MAIN CODE
function tiw_precio_cereales_bcr_deactivate() {
    global $wpdb;
    $tabla_data_precios_cereales = $wpdb->prefix . "data_tiw_precios_cereales_bcr"; 
    $sql = "DROP TABLE IF EXISTS $tabla_data_precios_cereales;";
    $wpdb->query($sql);
}
register_deactivation_hook( __FILE__, 'tiw_precio_cereales_bcr_deactivate' );



//REGISTER CSS
function add_precio_bcr_css() 
{
    wp_enqueue_style( 'precio_bcr_css', plugins_url( '/css/precio_bcr_css.css', __FILE__ ) );
}

//GET CSS
add_action('wp_print_styles', 'add_precio_bcr_css');

//ADD DATA TO DB
function add_data_cereales($dia_hoy, $trigo_hoy, $sorgo_hoy, $soja_hoy, $maiz_hoy){
	global $wpdb;
	$tabla_data_precios_cereales = $wpdb->prefix . "data_tiw_precios_cereales_bcr";

	$check_db_data = $wpdb->get_row("SELECT * FROM $tabla_data_precios_cereales WHERE dia = '$dia_hoy'");

	if(empty($check_db_data))
	{
		$wpdb->insert($tabla_data_precios_cereales, array(
				'dia' => $dia_hoy, 
				'trigo' => $trigo_hoy, 
				'sorgo' => $sorgo_hoy, 
				'soja' => $soja_hoy,
				'maiz' => $maiz_hoy
		));
		//echo $wpdb->last_query;
	}
}



//GET DATA TO DB
function get_data_cereales($grano){
	global $wpdb;

	$dia_ayer = date("zY", time() - 60 * 60 * 24);
	$grano = strtolower($grano);
	$tabla_data_precios_cereales = $wpdb->prefix . "data_tiw_precios_cereales_bcr";

	$data_cereales_ayer = $wpdb->get_results("SELECT $grano FROM $tabla_data_precios_cereales WHERE dia = '$dia_ayer'");
	$data_cereales_ayer = json_decode(json_encode($data_cereales_ayer), True);

	if(empty($data_cereales_ayer)){
		$data_cereales_ayer = $wpdb->get_results("SELECT $grano FROM $tabla_data_precios_cereales ORDER BY id DESC LIMIT 1 OFFSET 1");
		$data_cereales_ayer = json_decode(json_encode($data_cereales_ayer), True);	
	}
	
	foreach($data_cereales_ayer as $precio_grano)
	{	
		//var_dump($data_cereales_ayer);
		return $precio_grano[$grano];
	}
}

//WIDGET MAIN CODE
class tiw_precio_cereales_bcr extends WP_Widget {

	// constructor
	function tiw_precio_cereales_bcr() {
		parent::WP_Widget(false, $name = __('Precio Cereales BCR', 'wp_widget_plugin') );
	}

	// widget form creation
	function form($instance) {	
		// Check values
		if( $instance) {
		     $title = esc_attr($instance['title']);
		     //$mercado = esc_attr($instance['mercado']);
		} else {
		     $title = '';
		     //$mercado = '';
		}

		?>
			<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'wp_widget_plugin'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</p>
			
		<?php
	}

	// update widget
	function update($new_instance, $old_instance) {
	      $instance = $old_instance;
	      // Fields
	      $instance['title'] = strip_tags($new_instance['title']);
	      //$instance['mercado'] = strip_tags($new_instance['mercado']);
	     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

		if(empty($instance['mercado'])){ 
			$instance['mercado'] = "Rosario"; 
		}
		echo 'Precios de: '.$instance['mercado'];
		echo '<p>';
		check_bcr($instance['mercado']);
	}
}

//REGISTER WIDGET
add_action('widgets_init', create_function('', 'return register_widget("tiw_precio_cereales_bcr");'));


//GET COTIZACIONES MAIN PLUGIN CODE
function check_bcr($mercado) {

	global $wpdb;

	if(empty($mercado))
	{
		$mercado = "Rosario";
	}

	$bcr_wsdl = "http://services.bcr.com.ar/cotiza/wscotizaciones.asmx?WSDL";
	$bcr_client = new SoapClient($bcr_wsdl, array(
                            "trace"=>1,
                            "exceptions"=>0));
    $tipo = "";
    $parameters= array("request"=>$tipo);
    $value = $bcr_client->GetCotizaciones($parameters);
    
    $xml = $value->GetCotizacionesResult;

	$get_item = $xml->Precio;
	$main_array = json_decode(json_encode($get_item), True);

	$trigo = 0;
	$soja = 0;
	$sorgo = 0;
	$maiz = 0;
	$dia_hoy = date("zY");

	echo '<table border="0">';
	echo '<tr>';
	echo '<td><b>Grano</b></td>';
	echo '<td><b>Precio</b></td>';
	echo '</tr><tr>';

	foreach($main_array as $mini_array)
	{
		if(!empty($mini_array) AND $mini_array["Descripcion"] == $mercado AND $mini_array["Monto"] != "0")
		{	
			
			if($mini_array["Grano"] == "Trigo") { ++$trigo; }
			if($mini_array["Grano"] == "Soja") { ++$soja; }
			if($mini_array["Grano"] == "Sorgo") { ++$sorgo; }
			if($mini_array["Grano"] == "Maíz") { ++$maiz; }

			if($mini_array["Grano"] == "Trigo" AND $trigo < 2) 
			{
				echo '<td>'.$mini_array["Grano"].'</td>';
				echo '<td><b>'.$mini_array["Monto"].'</b>';
					$trigo_hoy = $mini_array["Monto"];
					if($mini_array["Movimiento"] == "+"){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/up.png>'); }
					if($mini_array["Movimiento"] == "-"){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/down.png>'); }
					if($mini_array["Movimiento"] == "="){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/equal.png>'); }
					$var_trigo = get_data_cereales($mini_array["Grano"]);
					$diferencia_trigo = $trigo_hoy - $var_trigo;
					if($diferencia_trigo != 0)
					{
						echo ' '.$diferencia_trigo;
					}
					
				echo '</td>';				
			}
			if($mini_array["Grano"] == "Soja" AND $soja < 2) 
			{
				echo '<td>'.$mini_array["Grano"].'</td>';
				echo '<td><b>'.$mini_array["Monto"].'</b>';
					$soja_hoy = $mini_array["Monto"];
					if($mini_array["Movimiento"] == "+"){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/up.png>'); }
					if($mini_array["Movimiento"] == "-"){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/down.png>'); }
					if($mini_array["Movimiento"] == "="){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/equal.png>'); }
					$var_soja = get_data_cereales($mini_array["Grano"]);
					$diferencia_soja = $soja_hoy - $var_soja;
					if($diferencia_soja != 0)
					{
						echo ' '.$diferencia_soja;					
					}
			}
			if($mini_array["Grano"] == "Sorgo" AND $sorgo < 2) 
			{
				echo '<td>'.$mini_array["Grano"].'</td>';
				echo '<td><b>'.$mini_array["Monto"].'</b>';
					$sorgo_hoy = $mini_array["Monto"];
					if($mini_array["Movimiento"] == "+"){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/up.png>'); }
					if($mini_array["Movimiento"] == "-"){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/down.png>'); }
					if($mini_array["Movimiento"] == "="){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/equal.png>'); }	
					$var_sorgo = get_data_cereales($mini_array["Grano"]);
					$diferencia_sorgo = $sorgo_hoy - $var_sorgo;
					if($diferencia_sorgo != 0)
					{	
						echo ' '.$diferencia_sorgo;					
					}
				echo '</td>';				
			}
			if($mini_array["Grano"] == "Maíz" AND $maiz < 2) 
			{
				echo '<td>'.$mini_array["Grano"].'</td>';
				echo '<td><b>'.$mini_array["Monto"].'</b>';
					$maiz_hoy = $mini_array["Monto"];
					$grano = "Maiz";
					if($mini_array["Movimiento"] == "+"){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/up.png>'); }
					if($mini_array["Movimiento"] == "-"){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/down.png>'); }
					if($mini_array["Movimiento"] == "="){ print (' &nbsp; <img src='.plugin_dir_url( __FILE__ ).'/img/equal.png>'); }			
					$var_maiz = get_data_cereales($grano);
					$diferencia_maiz = $maiz_hoy - $var_maiz;
					if($diferencia_maiz !=  0) 
					{
						echo ' '.$diferencia_maiz;	
					}				
				echo '</td>';				
			}									
		}
		echo '</tr>';
	}  

	echo '</table>';
	echo '<p class=""></p>';
	echo 'Datos extraidos de: <br> <a href="https://www.bcr.com.ar/Pages/Granos/Cotizaciones/default.aspx">Bolsa de Comercio de Rosario</a>';
	echo '<br><br>';

	add_data_cereales($dia_hoy, $trigo_hoy, $sorgo_hoy, $soja_hoy, $maiz_hoy);
}

add_shortcode('precios-cereales', 'check_bcr');
?>